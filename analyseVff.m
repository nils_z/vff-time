function analyseVff (measurementFolder, imageFolder, file)
  load(strcat(measurementFolder, '/', file))

  % Create time vector
  time = 0:Tinterval:(Length*Tinterval-Tinterval);

  % Hack: Cut vectors, sometimes one is too large, reason?
  A = A(1:length(time));
  B = B(1:length(time));
  
  % Hack: Set Inf values to biggest finite values
  A(find(~isfinite(A))) = max(A(find(isfinite(A))));
  B(find(~isfinite(B))) = max(B(find(isfinite(B))));

  % Remove DC
  A = A - mean(A);
  B = B - mean(B);

  % Find peaks, lower distance until at least one peak is found
  distances = [40000 30000 20000 15000 10000 8000 6000 5000 4000 3000];
  for dist = distances
    [pksA, locsA] = findpeaks(A, 'DoubleSided', 'MinPeakDistance', dist);
    if length(pksA) > 0
      break
    endif
  end;
  for dist = distances
    [pksB, locsB] = findpeaks(B, 'DoubleSided', 'MinPeakDistance', dist);
    if length(pksB) > 0
      break
    endif
  end;
  

  fprintf('Signal length is %.2fs.\n', time(end))
  fprintf('Found %d peaks in A and %d peaks in B.\n', length(pksA), length(pksB))
  
  f = figure;
  set(f, "visible", "off")

  % Plot signal A and B with markers at peaks
  subplot(2,2,1)
  plot(time, A)
  hold on
  plot(time(locsA),pksA,'ok', "markersize", 20)
  title('Signal A'); xlabel('t / s'); ylabel('U / V')
  grid on

  subplot(2,2,2)
  plot(time, B, 'g')
  hold on
  plot(time(locsB),pksB,'ok', "markersize", 20)
  title('Signal B'); xlabel('t / s'); ylabel('U / V')
  grid on

  % Some checks
  if(length(pksA) != length(pksB))
    print(strcat(imageFolder, '/', file, "-vff.png"), "-dpng")
    error('Error: Different number of peaks found und signals A and B. Exiting.')
  elseif(length(pksA) == 0)
    print(strcat(imageFolder, '/', file, "-vff.png"), "-dpng")
    error('Error: No peaks found. Exiting.')
  endif

  % Zoom in
  startTime = max(1, min(locsA(1), locsB(1)) - 6000);
  endTime = min(Length, max(locsA(end), locsB(end)) + 6000);

  % Find half height of peaks in A
  halfHeightLocsA = [];
  for pkLocA = locsA%(find(pksB > mean(B))) % only positive
    halfHeight = (A(pkLocA)+mean(A))/2;
    
    % Find all points with lower values than halfHeight left of the peak
    % Get the point out of this points, which is most right (nearest to the peak)
    % Note the different approach for positive and negative peaks
    if(halfHeight > 0) halfHeightLoc = max(find(find(A < halfHeight) < pkLocA));
    else halfHeightLoc = max(find(find(A > halfHeight) < pkLocA)); endif
    
    halfHeightLocsA(end+1) = halfHeightLoc;
  end

  % Find half height of peaks in B
  halfHeightLocsB = [];
  for pkLocB = locsB%(find(pksB > mean(B))) % only positive
    halfHeight = (B(pkLocB)+mean(B))/2;
    
    if(halfHeight > 0) halfHeightLoc = max(find(find(B < halfHeight) < pkLocB));
    else halfHeightLoc = max(find(find(B > halfHeight) < pkLocB)); endif
    
    halfHeightLocsB(end+1) = halfHeightLoc;
  end

  % Plot zoomed extract, mark peaks and halfHeight peaks
  subplot(2,1,2)
  plot(time(startTime:endTime), A(startTime:endTime))
  hold on
  plot(time(startTime:endTime), B(startTime:endTime), 'g')
  plot(time(locsA),pksA,'ok', "markersize", 20)
  plot(time(locsB),pksB,'ok', "markersize", 20)
  plot(time(halfHeightLocsA),A(halfHeightLocsA),'xr', "markersize", 20)
  plot(time(halfHeightLocsB),B(halfHeightLocsB),'xr', "markersize", 20)

  grid on
  xlim([startTime endTime]*Tinterval)
  title('Ausschnitt und Zeitmessung'); xlabel('t / s'); ylabel('U / V')

  % Measure distance between halfHeight peak in B and in A
  for j = 1:length(halfHeightLocsA)
    x = [time(halfHeightLocsB(j)) time(halfHeightLocsA(j))];
    y = [B(halfHeightLocsB(j)) B(halfHeightLocsB(j))];
    line(x, y);
    text(x(1)+0.01, y(1)+0.05,num2str(time(halfHeightLocsA(j)) - time(halfHeightLocsB(j))))
    text(x(1)-0.02, y(1), num2str(j))
    fprintf('Peak %2d: %fs\n', j, time(halfHeightLocsA(j)) - time(halfHeightLocsB(j)))
  end
  
  print(strcat(imageFolder, '/', file, "-vff.png"), "-dpng")
endfunction
