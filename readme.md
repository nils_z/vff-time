vff-time
=========

`main.m` mit Octave öffnen ausführen.

* Im Ordner `measurements` müssen die Rohdateien als .mat liegen.
* Im Ordner `images` liegt nach der Analyse ein Bild für jede ausgewertete Datei.
* Die Octave-Konsole zeigt zu jeder Datei einige Daten und evtl. Fehler an

### Beispiel:

    [vff-time]$ octave main.m
    GNU Octave, version 3.8.2
    
    file = 20141012-0007.mat
    Signal length is 12.58s.
    Found 1 peaks in A and 1 peaks in B.
    Peak  1: 0.076512s
    -----
    file = 4659_3.mat
    Signal length is 23.12s.
    Found 1 peaks in A and 2 peaks in B.
    Error: Different number of peaks found und signals A and B. Exiting.
    -----
    file = 4945_1.mat
    Signal length is 25.81s.
    Found 1 peaks in A and 1 peaks in B.
    Peak  1: 0.080352s
    -----
    
    [vff-time]$

![IMAGE](https://bytebucket.org/nils_z/vff-time/raw/master/images/4945_1.mat-vff.png)