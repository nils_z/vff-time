clear all
close all
pkg load signal
warning ("off", "Octave:broadcast");
graphics_toolkit("gnuplot")
more off

measurementFolder = 'measurements';
imageFolder = 'images';

filelist = readdir(measurementFolder);
for ii = 1:numel(filelist)
  ## skip special files . and ..
  if (regexp (filelist{ii}, "^\\.\\.?$"))
    continue;
  endif
  ## load your file
  file = filelist{ii}
  
  try
    analyseVff(measurementFolder, imageFolder, file)
    clf
  catch
    disp(lasterror.message)
  end_try_catch
  disp('-----')
endfor
